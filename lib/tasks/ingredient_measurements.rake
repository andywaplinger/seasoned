namespace :ingredient_measurements do

  desc "Add ingredient measurement names to db"
  task populate: :environment do

    
    #Imperial (US) Volume
    [
      [1, "drop", "imperial volume"],
      [2, "pinch", "imperial volume"],
      [3, "dash", "imperial volume"],
      [4, "tsp", "imperial volume"],
      [5, "tbsp", "imperial volume"],
      [6, "fl oz", "imperial volume"],
      [7, "shot", "imperial volume"],
      [8, "gill", "imperial volume"],
      [9, "cup", "imperial volume"],
      [10, "pint", "imperial volume"],
      [11, "quart", "imperial volume"],
      [12, "gallon", "imperial volume"],
      [13, "barrel", "imperial volume"]
    ].each do |id, name, system|
      ingredient_measurement = IngredientMeasurement.find_or_initialize_by(id: id)
      ingredient_measurement.update!(name: name, system: system)
    end

    #Imperial (US) Mass
    [
      [20, "lb", "imperial mass"],
      [21, "oz", "imperial mass"]
    ].each do |id, name, system|
      ingredient_measurement = IngredientMeasurement.find_or_initialize_by(id: id)
      ingredient_measurement.update!(name: name, system: system)
    end

    #Imperial (US) Length
    [
      [30, "in", "imperial length"],
      [31, "ft", "imperial length"]
    ].each do |id, name, system|
      ingredient_measurement = IngredientMeasurement.find_or_initialize_by(id: id)
      ingredient_measurement.update!(name: name, system: system)
    end

    #Metric Volume
    [
      [40, "mL", "metric volume"],
      [41, "L", "metric volume"]
    ].each do |id, name, system|
      ingredient_measurement = IngredientMeasurement.find_or_initialize_by(id: id)
      ingredient_measurement.update!(name: name, system: system)
    end

    IngredientMeasurement.find_by_id(42)&.delete

    #Metric Mass
    [
      [50, "mg", "metric mass"],
      [51, "g", "metric mass"],
      [52, "kg", "metric mass"]
    ].each do |id, name, system|
      ingredient_measurement = IngredientMeasurement.find_or_initialize_by(id: id)
      ingredient_measurement.update!(name: name, system: system)
    end

    #Metric Length
    [
      [60, "mm", "metric length"],
      [61, "cm", "metric length"]
    ].each do |id, name, system|
      ingredient_measurement = IngredientMeasurement.find_or_initialize_by(id: id)
      ingredient_measurement.update!(name: name, system: system)
    end

    #Generic units
    [
      [100, "", "generic"],
      [101, "piece", "generic"]
    ].each do |id, name, system|
      ingredient_measurement = IngredientMeasurement.find_or_initialize_by(id: id)
      ingredient_measurement.update!(name: name, system: system)
    end

    puts "#{'*'*(`tput cols`.to_i)}\nThe database has been populated!\n#{'*'*(`tput cols`.to_i)}"
  end

end
