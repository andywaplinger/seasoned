Rails.application.routes.draw do

  root to: redirect("/earlyaccess")
  #root to: "recipes#index"

  devise_for :users,
             :path => "",
             :path_names => {
              :sign_in => 'login',
              :sign_out => 'logout',
              :sign_up => 'signup' },
             controllers: { registrations: "users/registrations", sessions: "users/sessions" }

  resources :recipes, except: [:update, :destroy, :edit]
  namespace :recipes do
    resources :v, controller: "/recipe_versions", except: [:new, :create] do
      resources :versions, controller: "/recipe_versions", only: [:new, :create]
    end
  end

  get "*path", to: redirect("/404")
end
