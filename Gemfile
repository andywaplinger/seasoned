source 'https://rubygems.org'
ruby "2.5.1"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0'
# Use PostgreSQL as the database for Active Record
gem 'pg', '~> 1.0'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sassc-rails', '~> 2.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '~> 4.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'sprockets-rails', :require => 'sprockets/railtie'
gem 'bootstrap', '~> 4.0'
gem 'jquery-rails', '~> 4.0'
gem 'vuejs-rails', '~> 2.0'
gem 'gon', '~> 6.0'
gem 'lodash-rails', '~> 4.0'

gem 'devise', '~> 4.0'
gem 'activerecord-import', '~> 0.0'

gem 'inline_svg', '~> 1.0'
gem 'font-awesome-sass', '~> 5.0'
gem 'invisible_captcha', '~> 0.0'
gem 'hashid-rails', '~> 1.0'

gem 'sendgrid', '~> 1.0'
gem 'sendgrid-ruby', '~> 5.0'
gem 'sendgrid-actionmailer', '~> 2.0'

group :staging, :production do
end

group :development, :test do
  # Use sqlite3 as the database for Active Record
  #gem 'sqlite3', '~> 1.3'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.0'
  gem 'selenium-webdriver', '~> 3.0'
  gem 'rspec-rails', '~> 3.0'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '~> 2.0'
  gem 'spring-watcher-listen', '~> 2.0'

  gem 'solargraph', '~> 0.0'

  gem 'rails_layout', '~> 1.0'
  gem 'better_errors', '~> 2.0'
  gem 'binding_of_caller', '~> 0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data'
