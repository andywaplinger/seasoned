var animation = bodymovin.loadAnimation({
  container: document.getElementById('seasoned_versions'),
  renderer: 'svg',
  loop: true,
  autoplay: true,
  path: 'img/animation/seasoned_versions.json'
})
