class CreateIngredients < ActiveRecord::Migration[5.1]
  def change
    create_table :ingredients do |t|
      
      enable_extension("citext")

      t.citext :name, null: false
      t.index :name, unique: true
    end
  end
end
