class CreateRecipeVersions < ActiveRecord::Migration[5.1]
  def change
    create_table :recipe_versions do |t|
      t.belongs_to :recipe, null: false, index: true, foreign_key: true
      t.belongs_to :parent_recipe_version, null: false, index: true
      t.belongs_to :user, null: false, index: true, foreign_key: true
      t.text :description
      t.datetime :locked_at, index: true
      t.timestamps null: false, index: true
    end
  end
end
