class CreateIngredientMeasurements < ActiveRecord::Migration[5.1]
  def change
    create_table :ingredient_measurements do |t|
      t.string :name, null: false
      t.string :system, null: false
      t.index [:name, :system], unique: true
    end
    Rake::Task['ingredient_measurements:populate'].invoke
  end
end
