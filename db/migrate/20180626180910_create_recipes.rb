class CreateRecipes < ActiveRecord::Migration[5.1]
  def change
    create_table :recipes do |t|
      t.belongs_to :user, null: false, index: true, foreign_key: true
      t.string :title, null: false, index: true
      t.timestamps null: false, index: true
    end
  end
end
