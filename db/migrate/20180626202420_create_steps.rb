class CreateSteps < ActiveRecord::Migration[5.1]
  def change
    create_table :steps do |t|
      t.string :content, null: false, limit: 800
      t.index :content, unique: true
    end
  end
end
