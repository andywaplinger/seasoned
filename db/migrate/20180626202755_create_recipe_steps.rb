class CreateRecipeSteps < ActiveRecord::Migration[5.1]
  def change
    create_table :recipe_steps do |t|
      t.belongs_to :recipe_version, null: false, foreign_key: true
      t.belongs_to :step, null: false, index: true, foreign_key: true
      t.integer :position, null: false
      t.timestamps null: false, index: true
      t.index [:recipe_version_id, :position], unique: true
    end
  end
end
