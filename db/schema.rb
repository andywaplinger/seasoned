# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_03_030401) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "citext"
  enable_extension "plpgsql"

  create_table "ingredient_measurements", force: :cascade do |t|
    t.string "name", null: false
    t.string "system", null: false
    t.index ["name", "system"], name: "index_ingredient_measurements_on_name_and_system", unique: true
  end

  create_table "ingredients", force: :cascade do |t|
    t.citext "name", null: false
    t.index ["name"], name: "index_ingredients_on_name", unique: true
  end

  create_table "recipe_ingredients", force: :cascade do |t|
    t.bigint "recipe_version_id", null: false
    t.bigint "ingredient_id", null: false
    t.decimal "amount", precision: 6, scale: 2, null: false
    t.bigint "ingredient_measurement_id", null: false
    t.integer "position", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_at"], name: "index_recipe_ingredients_on_created_at"
    t.index ["ingredient_id"], name: "index_recipe_ingredients_on_ingredient_id"
    t.index ["ingredient_measurement_id"], name: "index_recipe_ingredients_on_ingredient_measurement_id"
    t.index ["recipe_version_id", "position"], name: "index_recipe_ingredients_on_recipe_version_id_and_position", unique: true
    t.index ["recipe_version_id"], name: "index_recipe_ingredients_on_recipe_version_id"
    t.index ["updated_at"], name: "index_recipe_ingredients_on_updated_at"
  end

  create_table "recipe_steps", force: :cascade do |t|
    t.bigint "recipe_version_id", null: false
    t.bigint "step_id", null: false
    t.integer "position", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_at"], name: "index_recipe_steps_on_created_at"
    t.index ["recipe_version_id", "position"], name: "index_recipe_steps_on_recipe_version_id_and_position", unique: true
    t.index ["recipe_version_id"], name: "index_recipe_steps_on_recipe_version_id"
    t.index ["step_id"], name: "index_recipe_steps_on_step_id"
    t.index ["updated_at"], name: "index_recipe_steps_on_updated_at"
  end

  create_table "recipe_versions", force: :cascade do |t|
    t.bigint "recipe_id", null: false
    t.bigint "parent_recipe_version_id", null: false
    t.bigint "user_id", null: false
    t.text "description"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_at"], name: "index_recipe_versions_on_created_at"
    t.index ["locked_at"], name: "index_recipe_versions_on_locked_at"
    t.index ["parent_recipe_version_id"], name: "index_recipe_versions_on_parent_recipe_version_id"
    t.index ["recipe_id"], name: "index_recipe_versions_on_recipe_id"
    t.index ["updated_at"], name: "index_recipe_versions_on_updated_at"
    t.index ["user_id"], name: "index_recipe_versions_on_user_id"
  end

  create_table "recipes", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "title", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_at"], name: "index_recipes_on_created_at"
    t.index ["title"], name: "index_recipes_on_title"
    t.index ["updated_at"], name: "index_recipes_on_updated_at"
    t.index ["user_id"], name: "index_recipes_on_user_id"
  end

  create_table "steps", force: :cascade do |t|
    t.string "content", limit: 800, null: false
    t.index ["content"], name: "index_steps_on_content", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "recipe_ingredients", "ingredient_measurements"
  add_foreign_key "recipe_ingredients", "ingredients"
  add_foreign_key "recipe_ingredients", "recipe_versions"
  add_foreign_key "recipe_steps", "recipe_versions"
  add_foreign_key "recipe_steps", "steps"
  add_foreign_key "recipe_versions", "recipes"
  add_foreign_key "recipe_versions", "users"
  add_foreign_key "recipes", "users"
end
