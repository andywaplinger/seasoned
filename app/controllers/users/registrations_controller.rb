class Users::RegistrationsController < Devise::RegistrationsController
  invisible_captcha only: [:create], honeypot: :name

  protected

  def after_sign_up_path_for(resource)
    "/login"
  end

  def after_inactive_sign_up_path_for(resource)
    "/login"
  end
end
