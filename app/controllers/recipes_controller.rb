class RecipesController < AuthenticatedController

  def index
    @recipes = current_user.recipes
  end

  def show
    @recipe = current_user.recipes.find_by_hashid(params[:id])
  end

  def new
    @recipe = Recipes::NewForm.new.recipe
    gon.recipe = Recipes::NewForm.new.gon
  end

  def edit
  end

  def create
    form = Recipes::CreateForm.new(params: params, user: current_user)
    if form.save
      redirect_to action: :show, id: form.recipe.hashid
    else
      @recipe = form.recipe
      gon.recipe = form.gon
      render :new
    end
  end

  def update
  end

  def destroy
  end
end
