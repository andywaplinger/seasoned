class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  rescue_from ActiveRecord::RecordNotFound, with: :show_error!

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [
        :first_name,
        :last_name,
        :email,
        :password,
        :password_confirmation
      ])

    devise_parameter_sanitizer.permit(:account_update, keys: [
        :first_name,
        :last_name,
        :email,
        :current_password,
        :password,
        :password_confirmation
      ])
  end

  def show_error
    respond_to do |format|
      format.html { redirect_to "/401" }
      format.json { head 401 }
    end
  end
end
