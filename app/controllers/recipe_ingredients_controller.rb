class RecipeIngredientsController < AuthenticatedController
  def create
    @recipe_ingredient = RecipeIngredient.new(recipe_ingredient_params)
  end

  private

  def recipe_ingredient_params
    params.require(:recipe_versions[0]).permit(
      recipe_ingredients: [:amount,
      :ingredient_measurement_id,
      :ingredient])
  end
end
