class RecipeVersionsController < AuthenticatedController
  def index
  end

  def show
    @recipe_version = RecipeVersion.where(user: current_user).find_by_hashid!(params[:id])
  end

  def new
    @recipe_version = RecipeVersion.locked.where(user: current_user).find_by_hashid!(params[:v_id])
    @recipe_version.assign_attributes(parent_recipe_version_id: @recipe_version.id)
    gon.recipe_version = RecipeVersions::NewForm.new(@recipe_version).gon
  end

  def edit
    @recipe_version = RecipeVersion.unlocked.where(user: current_user).find_by_hashid!(params[:id])
    gon.recipe_version = RecipeVersions::NewForm.new(@recipe_version).gon
  end

  def create
    form = RecipeVersions::CreateForm.new(
      parent_recipe_version: parent_recipe_version, params: params, user: current_user
    )
    if form.save
      redirect_to action: :show, id: form.recipe_version.hashid
    else
      @recipe_version = form.recipe_version
      gon.recipe_version = form.gon
      render :new
    end
  end

  def update
    form = RecipeVersions::UpdateForm.new(
      recipe_version_hashid: params[:id], params: params, user: current_user
    )
    if form.save
      redirect_to action: :show, id: form.recipe_version.hashid
    else
      @recipe_version = form.recipe_version
      gon.recipe_version = form.gon
      render :edit
    end
  end

  def destroy
  end

  private

  def parent_recipe_version
    @parent_recipe_version ||=  RecipeVersion.locked
                                             .where(user: current_user)
                                             .find_by_hashid!(params[:v_id])
  end

  def recipe_version
    @recipe_version ||= RecipeVersion.unlocked
                                     .where(user: current_user)
                                     .find_by_hashid!(params[:v_id])
  end
end
