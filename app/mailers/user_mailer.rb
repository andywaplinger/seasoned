class UserMailer < Devise::Mailer
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views

  require 'sendgrid-ruby'
  include SendGrid
  require 'json'

  def confirmation_instructions(record, token, opts={})
    if record.pending_reconfirmation?
      mail = Mail.new
      mail.template_id = '3aca9f0d-9756-4041-99b7-9488d986836a'
      mail.from = Email.new(email: 'admin@seasonedapp.com')
      mail.reply_to = Email.new(email: 'admin@seasonedapp.com')
      mail.add_category(Category.new(name: 'email change confirmation'))

      personalization = Personalization.new
      personalization.add_to(Email.new(email: record.email, name: record.first_name))
      personalization.add_substitution(Substitution.new(key: '%new_email%', value: record.unconfirmed_email))
      personalization.add_substitution(Substitution.new(key: '%confirmation_url%', value: confirmation_url(record, confirmation_token: token)))
      mail.add_personalization(personalization)

      tracking_settings = TrackingSettings.new
      tracking_settings.click_tracking = ClickTracking.new(enable: true, enable_text: false)
      tracking_settings.open_tracking = OpenTracking.new(enable: true)
      mail.tracking_settings = tracking_settings

      sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
      begin
        response = sg.client.mail._("send").post(request_body: mail.to_json)
      rescue Exception => e
        puts e.message
      end
      puts response.status_code
      puts response.body
      puts response.headers
    else
      mail = Mail.new
      mail.template_id = 'b2f2a48c-2df0-4d2b-b23e-d99819fc5395'
      mail.from = Email.new(email: 'admin@seasonedapp.com')
      mail.reply_to = Email.new(email: 'admin@seasonedapp.com')
      mail.add_category(Category.new(name: 'signup confirmation'))

      personalization = Personalization.new
      personalization.add_to(Email.new(email: record.email, name: record.first_name))
      personalization.add_substitution(Substitution.new(key: '%confirmation_url%', value: confirmation_url(record, confirmation_token: token)))
      personalization.add_substitution(Substitution.new(key: '%first_name%', value: record.first_name))
      mail.add_personalization(personalization)

      tracking_settings = TrackingSettings.new
      tracking_settings.click_tracking = ClickTracking.new(enable: true, enable_text: false)
      tracking_settings.open_tracking = OpenTracking.new(enable: true)
      mail.tracking_settings = tracking_settings

      sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
      begin
        response = sg.client.mail._("send").post(request_body: mail.to_json)
      rescue Exception => e
        puts e.message
      end
      puts response.status_code
      puts response.body
      puts response.headers
    end
  end

  def email_changed(record, opts={})
    if record.confirmed_at.present? && record.unconfirmed_email.blank?
      mail = Mail.new
      mail.template_id = '74620df9-7688-4bda-b137-9e6b8837207d'
      mail.from = Email.new(email: 'admin@seasonedapp.com')
      mail.reply_to = Email.new(email: 'admin@seasonedapp.com')
      mail.add_category(Category.new(name: 'email change notification'))

      personalization = Personalization.new
      personalization.add_to(Email.new(email: record.email, name: record.first_name))
      mail.add_personalization(personalization)

      tracking_settings = TrackingSettings.new
      tracking_settings.click_tracking = ClickTracking.new(enable: true, enable_text: false)
      tracking_settings.open_tracking = OpenTracking.new(enable: true)
      mail.tracking_settings = tracking_settings

      sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
      begin
        response = sg.client.mail._("send").post(request_body: mail.to_json)
      rescue Exception => e
        puts e.message
      end
      puts response.status_code
      puts response.body
      puts response.headers
    end
  end

  def reset_password_instructions(record, token, opts={})
    mail = Mail.new
    mail.template_id = 'dd208914-6b48-431d-8db5-56bb2f0b3768'
    mail.from = Email.new(email: 'admin@seasonedapp.com')
    mail.reply_to = Email.new(email: 'admin@seasonedapp.com')
    mail.add_category(Category.new(name: 'password reset instruction'))

    personalization = Personalization.new
    personalization.add_to(Email.new(email: record.email, name: record.first_name))
    personalization.add_substitution(Substitution.new(key: '%reset_url%', value: edit_user_password_url(record, reset_password_token: token)))
    mail.add_personalization(personalization)

    tracking_settings = TrackingSettings.new
    tracking_settings.click_tracking = ClickTracking.new(enable: true, enable_text: false)
    tracking_settings.open_tracking = OpenTracking.new(enable: true)
    mail.tracking_settings = tracking_settings

    sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
    begin
      puts mail.to_json
      response = sg.client.mail._("send").post(request_body: mail.to_json)
    rescue Exception => e
      puts e.message
    end
    puts response.status_code
    puts response.body
    puts response.headers
  end

  def password_change(record, opts={})
    mail = Mail.new
    mail.template_id = 'd022a20a-23c4-45de-8df5-74547df62c22'
    mail.from = Email.new(email: 'admin@seasonedapp.com')
    mail.reply_to = Email.new(email: 'admin@seasonedapp.com')
    mail.add_category(Category.new(name: 'password change confirmation'))

    personalization = Personalization.new
    personalization.add_to(Email.new(email: record.email, name: record.first_name))
    mail.add_personalization(personalization)

    tracking_settings = TrackingSettings.new
    tracking_settings.click_tracking = ClickTracking.new(enable: true, enable_text: false)
    tracking_settings.open_tracking = OpenTracking.new(enable: true)
    mail.tracking_settings = tracking_settings

    sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
    begin
      response = sg.client.mail._("send").post(request_body: mail.to_json)
    rescue Exception => e
      puts e.message
    end
    puts response.status_code
    puts response.body
    puts response.headers
  end
end
