class ApplicationMailer < ActionMailer::Base
  default from: 'admin@seasonedapp.com'
  layout 'mailer'
end
