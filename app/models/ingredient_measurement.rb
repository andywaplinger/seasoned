class IngredientMeasurement < ApplicationRecord

  def self.default_measurement
    @@default_measurement ||= IngredientMeasurement.find_by_name("")
  end

  def self.options_for_select
    pluck(Arel.sql("DISTINCT system")).map do |system|
      options = where(system: system).order(:id).select(:name, :id)
      [system, options]
    end.to_h
  end

  def self.options_as_select
    @options_as_select ||= pluck(Arel.sql("DISTINCT system")).map do |system|
      options = where(system: system).order(:id).pluck(:name, :id)
      [system, options]
    end.to_h
  end
end
