class Recipe < ApplicationRecord
  include Hashid::Rails

  has_many :recipe_versions, dependent: :destroy
  belongs_to :user

  validates :recipe_versions, length: { minimum: 1 }
  validates_presence_of :title
  validates_associated :recipe_versions

  accepts_nested_attributes_for :recipe_versions
end
