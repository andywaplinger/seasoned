class Step < ApplicationRecord
  has_many :recipe_steps

  validates :content, presence: true, length: { minimum: 1 }, uniqueness: true
end
