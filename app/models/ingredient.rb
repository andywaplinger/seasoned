class Ingredient < ApplicationRecord
  has_many :recipe_ingredients

  validates :name, presence: true, length: { minimum: 1 }, uniqueness: true
end
