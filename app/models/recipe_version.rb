class RecipeVersion < ApplicationRecord
  include Hashid::Rails

  scope :unlocked, -> { where(locked_at: nil) }
  scope :locked, -> { where.not(locked_at: nil) }

  belongs_to :user
  belongs_to :recipe
  belongs_to :parent_recipe_version, class_name: "RecipeVersion", optional: true
  has_many :recipe_ingredients, -> { order(position: :asc) }, inverse_of: :recipe_version, dependent: :destroy
  has_many :recipe_steps, -> { order(position: :asc) }, inverse_of: :recipe_version, dependent: :destroy
  has_one :child_recipe_version, class_name: "RecipeVersion", foreign_key: :parent_recipe_version_id

  validates_presence_of :parent_recipe_version_id
  validates_associated :recipe_ingredients, :recipe_steps
  validates :recipe_steps, length: { minimum: 1 }
  validates :recipe_ingredients, length: { minimum: 1 }
  validate :not_unlocking_version
  
  before_validation :reorder_steps
  before_validation :reorder_ingredients

  accepts_nested_attributes_for :recipe_ingredients, allow_destroy: true
  accepts_nested_attributes_for :recipe_steps, allow_destroy: true

  def locked=(value)
    if value
      self.locked_at = Time.now
    else
      self.locked_at = nil
    end
  end

  private

  def reorder_steps
    self.recipe_steps.each.with_index do |recipe_step, index|
      recipe_step.position = index
    end
  end

  def reorder_ingredients
    self.recipe_ingredients.each.with_index do |recipe_ingredient, index|
      recipe_ingredient.position = index
    end
  end

  def not_unlocking_version
    if self.locked_at_was.present? && self.locked_at.blank?
      errors.add(:base, "Cannot unlock a version once it's locked.")
    end
  end
end
