class RecipeStep < ApplicationRecord
  belongs_to :recipe_version
  belongs_to :step

  validates_presence_of :position
  validates_associated :step

  accepts_nested_attributes_for :step
end
