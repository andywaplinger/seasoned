class RecipeIngredient < ApplicationRecord
  belongs_to :recipe_version
  belongs_to :ingredient
  belongs_to :ingredient_measurement

  validates_presence_of :amount, :position
  validates_associated :ingredient

  accepts_nested_attributes_for :ingredient

  after_initialize :set_defaults

  private

  def set_defaults
    self.ingredient_measurement_id ||= IngredientMeasurement.default_measurement.id
  end
end
