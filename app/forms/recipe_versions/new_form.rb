class RecipeVersions::NewForm
  attr_reader :recipe_version

  def initialize(recipe_version)
    @recipe_version = recipe_version
  end

  def gon
    recipe_version.serializable_hash(only: :description, include: {
      recipe: { only: [:title] },
      recipe_steps: { only: [:id], include: { step: { only: [:content] } } },
      recipe_ingredients: {
        only: [:id, :amount, :ingredient_measurement_id],
        include: {
          ingredient: { only: [:name] }
        }
      }
    })
  end
end
