class RecipeVersions::UpdateForm
  include ActiveModel::Model

  attr_accessor :params, :user, :recipe_version_hashid

  def save
    recipe_version.assign_attributes(recipe_version_params)
    transaction_save
  end

  def recipe_version
    @recipe_version ||= RecipeVersion.find_by_hashid!(recipe_version_hashid)
  end

  def gon
    rv = RecipeVersion.find_by_hashid!(recipe_version_hashid)
    rv.assign_attributes(recipe_version_params)
    rv.serializable_hash(only: :description, include: {
      recipe: { only: [:title] },
      recipe_steps: { only: [:id], include: { step: { only: [:content] } } },
      recipe_ingredients: {
        only: [:id, :amount, :ingredient_measurement_id],
        include: {
          ingredient: { only: [:name] }
        }
      }
    })
  end

  private

  def recipe_version_params
    params.require(:recipe_version).permit(
      :locked,
      :description,
      recipe_ingredients_attributes: [
        :id,
        :_destroy,
        :amount,
        :ingredient_measurement_id,
        { ingredient_attributes: [:name] }
      ],
      recipe_steps_attributes: [
        :id,
        :_destroy,
        { step_attributes: [:content] }
      ]
    )
  end

  def transaction_save
    ApplicationRecord.transaction do
      import_ingredients
      import_instructions
      recipe_version.save!
    end
    return true
  rescue => error
    puts error
    recipe_version.errors.add(:base, "There was an error saving this recipe version")
    false
  end

  def import_ingredients
    Ingredient.import ingredients, on_duplicate_key_ignore: true
    recipe_version.recipe_ingredients.each do |recipe_ingredient|
      ingredient = recipe_ingredient.ingredient
      if ingredient.id.blank?
        recipe_ingredient.ingredient = Ingredient.find_by_name ingredient.name
      end
      recipe_ingredient.ingredient_id = recipe_ingredient.ingredient.id
    end
  end

  def import_instructions
    Step.import steps, on_duplicate_key_ignore: true
    recipe_version.recipe_steps.each do |recipe_step|
      step = recipe_step.step
      if step.id.blank?
        recipe_step.step = Step.find_by_content step.content
      end
      recipe_step.step_id = recipe_step.step.id
    end
  end

  def ingredients
    recipe_version.recipe_ingredients.map(&:ingredient)
  end

  def steps
    recipe_version.recipe_steps.map(&:step)
  end
end
