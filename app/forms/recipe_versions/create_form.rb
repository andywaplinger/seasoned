class RecipeVersions::CreateForm
  include ActiveModel::Model

  attr_accessor :params, :user, :parent_recipe_version

  def save
    recipe_version.assign_attributes(
      recipe_id: parent_recipe_version.recipe_id,
      parent_recipe_version_id: parent_recipe_version.id,
      user: user
    )

    transaction_save
  end

  def recipe_version
    @recipe_version ||= RecipeVersion.new(recipe_version_params)
  end

  def gon
    RecipeVersion.new(recipe_version_params).serializable_hash(only: :description, include: {
      recipe: { only: [:title] },
      recipe_steps: { only: [], include: { step: { only: [:content] } } },
      recipe_ingredients: {
        only: [:amount, :ingredient_measurement_id],
        include: {
          ingredient: { only: [:name] }
        }
      }
    })
  end

  private

  def recipe_version_params
    params.require(:recipe_version).permit(
      :description,
      recipe_ingredients_attributes: [
        :amount,
        :ingredient_measurement_id,
        { ingredient_attributes: [:name] }
      ],
      recipe_steps_attributes: [
        { step_attributes: [:content] }
      ]
    )
  end

  def transaction_save
    ApplicationRecord.transaction do
      import_ingredients
      import_instructions
      recipe_version.save!
    end
    return true
  rescue => error
    puts error
    recipe_version.errors.add(:base, "There was an error saving this recipe version")
    false
  end

  def import_ingredients
    Ingredient.import ingredients, on_duplicate_key_ignore: true
    recipe_version.recipe_ingredients.each do |recipe_ingredient|
      ingredient = recipe_ingredient.ingredient
      if ingredient.id.blank?
        recipe_ingredient.ingredient = Ingredient.find_by_name ingredient.name
      end
      recipe_ingredient.ingredient_id = recipe_ingredient.ingredient.id
    end
  end

  def import_instructions
    Step.import steps, on_duplicate_key_ignore: true
    recipe_version.recipe_steps.each do |recipe_step|
      step = recipe_step.step
      if step.id.blank?
        recipe_step.step = Step.find_by_content step.content
      end
      recipe_step.step_id = recipe_step.step.id
    end
  end

  def ingredients
    recipe_version.recipe_ingredients.map(&:ingredient)
  end

  def steps
    recipe_version.recipe_steps.map(&:step)
  end
end
