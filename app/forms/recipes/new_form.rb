class Recipes::NewForm
  def recipe
    @recipe ||= Recipe.new(recipe_versions: [recipe_version])
  end

  def gon
    recipe.serializable_hash(only: [:title], include: {
      recipe_versions: { only: [:description], include: {
        recipe_steps: { only: [], include: { step: { only: [:content] } } },
        recipe_ingredients: { only: [:amount, :ingredient_measurement_id], include: {
          ingredient: { only: [:name] }
        }}
      }}
    })
  end

  private

  def recipe_version
    RecipeVersion.new(
      recipe_steps: [recipe_step], recipe_ingredients: [recipe_ingredient]
    )
  end

  def recipe_ingredient
    RecipeIngredient.new(ingredient: Ingredient.new)
  end

  def recipe_step
    RecipeStep.new(step: Step.new)
  end
end
