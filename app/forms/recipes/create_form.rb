class Recipes::CreateForm
  include ActiveModel::Model

  attr_accessor :params, :user

  validates :recipe_versions, length: { is: 1 }

  def save
    recipe_version.assign_attributes(
      locked_at: DateTime.now,
      parent_recipe_version_id: 0,
      user: user
    )
    valid? && transaction_save
  end

  def recipe
    @recipe ||= Recipe.new(recipe_params.merge(user: user))
  end

  def gon
    Recipe.new(recipe_params.merge(user: user)).serializable_hash(only: [:title], include: {
      recipe_versions: { only: [:description], include: {
        recipe_steps: { only: [], include: { step: { only: [:content] } } },
        recipe_ingredients: { only: [:amount, :ingredient_measurement_id], include: {
          ingredient: { only: [:name] }
        }}
      }}
    })
  end

  private

  def recipe_params
    params.require(:recipe).permit(
      :title,
      recipe_versions_attributes: [
        :description,
        {
          recipe_steps_attributes: [
            { step_attributes: [:content] }
          ],
          recipe_ingredients_attributes: [
            :amount,
            :ingredient_measurement_id,
            { ingredient_attributes: [:name] }
          ]
        }
      ])
  end

  def transaction_save
    ApplicationRecord.transaction do
      import_ingredients
      import_instructions
      recipe.save!
    end
    return true
  rescue => error
    puts error
    recipe.errors.add(:base, "There was an error saving the recipe")
    false
  end

  def import_ingredients
    Ingredient.import ingredients, on_duplicate_key_ignore: true
    recipe_version.recipe_ingredients.each do |recipe_ingredient|
      ingredient = recipe_ingredient.ingredient
      if ingredient.id.blank?
        recipe_ingredient.ingredient = Ingredient.find_by_name ingredient.name
      end
      recipe_ingredient.ingredient_id = recipe_ingredient.ingredient.id
    end
  end

  def import_instructions
    Step.import steps, on_duplicate_key_ignore: true
    recipe_version.recipe_steps.each do |recipe_step|
      step = recipe_step.step
      if step.id.blank?
        recipe_step.step = Step.find_by_content step.content
      end
      recipe_step.step_id = recipe_step.step.id
    end
  end

  def recipe_versions
    @recipe_versions ||= recipe.recipe_versions
  end

  def recipe_version
    @recipe_version ||= recipe_versions.first
  end

  def ingredients
    recipe_version.recipe_ingredients.map(&:ingredient)
  end

  def steps
    recipe_version.recipe_steps.map(&:step)
  end
end
